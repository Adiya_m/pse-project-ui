﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      Polu Varshith
// </reviewer>
// <date>
//      07-Nov-2018
// </date>
// <summary>
//      System generated code for UI design
// </summary>
// <copyright file="ServerChatScreen.Designer.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace MessagingUIServer
{
    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    partial class ServerChatScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        //// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerChatScreen));
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.toolStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.appLogoImageBox = new System.Windows.Forms.PictureBox();
            this.appLogoLabel = new System.Windows.Forms.Label();
            this.portLabel = new System.Windows.Forms.Label();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.portListenButton = new System.Windows.Forms.Button();
            this.chatFooterPanel = new System.Windows.Forms.Panel();
            this.shareScreenButton = new System.Windows.Forms.Button();
            this.sendButton = new System.Windows.Forms.Button();
            this.serverMessageTextBox = new System.Windows.Forms.RichTextBox();
            this.ServerChatSectionTabs = new System.Windows.Forms.TabControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.messageDisplayArea = new System.Windows.Forms.RichTextBox();
            this.statusBar.SuspendLayout();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.appLogoImageBox)).BeginInit();
            this.chatFooterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(121)))), ((int)(((byte)(107)))));
            this.statusBar.Dock = System.Windows.Forms.DockStyle.None;
            this.statusBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 648);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(211, 25);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // toolStripLabel
            // 
            this.toolStripLabel.ForeColor = System.Drawing.Color.White;
            this.toolStripLabel.Name = "toolStripLabel";
            this.toolStripLabel.Size = new System.Drawing.Size(207, 20);
            this.toolStripLabel.Text = "Current Session ID: ABCD1234";
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(182)))), ((int)(((byte)(159)))));
            this.headerPanel.Controls.Add(this.appLogoImageBox);
            this.headerPanel.Controls.Add(this.appLogoLabel);
            this.headerPanel.Controls.Add(this.portLabel);
            this.headerPanel.Controls.Add(this.portTextBox);
            this.headerPanel.Controls.Add(this.portListenButton);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1262, 60);
            this.headerPanel.TabIndex = 2;
            // 
            // appLogoImageBox
            // 
            this.appLogoImageBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.appLogoImageBox.Location = new System.Drawing.Point(11, 13);
            this.appLogoImageBox.Name = "appLogoImageBox";
            this.appLogoImageBox.Size = new System.Drawing.Size(115, 32);
            this.appLogoImageBox.TabIndex = 5;
            this.appLogoImageBox.TabStop = false;
            // 
            // appLogoLabel
            // 
            this.appLogoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.appLogoLabel.AutoSize = true;
            this.appLogoLabel.Font = new System.Drawing.Font("Segoe UI", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appLogoLabel.Location = new System.Drawing.Point(132, 6);
            this.appLogoLabel.Name = "appLogoLabel";
            this.appLogoLabel.Size = new System.Drawing.Size(357, 51);
            this.appLogoLabel.TabIndex = 4;
            this.appLogoLabel.Text = "MASTI Application";
            // 
            // portLabel
            // 
            this.portLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.portLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portLabel.ForeColor = System.Drawing.Color.White;
            this.portLabel.Location = new System.Drawing.Point(994, 11);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(53, 28);
            this.portLabel.TabIndex = 3;
            this.portLabel.Text = "PORT";
            this.portLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // portTextBox
            // 
            this.portTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.portTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(182)))), ((int)(((byte)(159)))));
            this.portTextBox.ForeColor = System.Drawing.Color.White;
            this.portTextBox.Location = new System.Drawing.Point(1053, 12);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(116, 34);
            this.portTextBox.TabIndex = 2;
            this.portTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PortNumberCharacterEntry);
            // 
            // portListenButton
            // 
            this.portListenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.portListenButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(29)))), ((int)(((byte)(58)))));
            this.portListenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.portListenButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portListenButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.portListenButton.Location = new System.Drawing.Point(1175, 12);
            this.portListenButton.Name = "portListenButton";
            this.portListenButton.Size = new System.Drawing.Size(75, 29);
            this.portListenButton.TabIndex = 1;
            this.portListenButton.Text = "LISTEN";
            this.portListenButton.UseVisualStyleBackColor = false;
            this.portListenButton.Click += new System.EventHandler(this.PortButtonClick);
            // 
            // chatFooterPanel
            // 
            this.chatFooterPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chatFooterPanel.Controls.Add(this.shareScreenButton);
            this.chatFooterPanel.Controls.Add(this.sendButton);
            this.chatFooterPanel.Controls.Add(this.serverMessageTextBox);
            this.chatFooterPanel.Location = new System.Drawing.Point(0, 594);
            this.chatFooterPanel.Name = "chatFooterPanel";
            this.chatFooterPanel.Size = new System.Drawing.Size(1262, 54);
            this.chatFooterPanel.TabIndex = 3;
            // 
            // shareScreenButton
            // 
            this.shareScreenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shareScreenButton.Enabled = false;
            this.shareScreenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shareScreenButton.Image = ((System.Drawing.Image)(resources.GetObject("shareScreenButton.Image")));
            this.shareScreenButton.Location = new System.Drawing.Point(1133, 3);
            this.shareScreenButton.Name = "shareScreenButton";
            this.shareScreenButton.Size = new System.Drawing.Size(60, 48);
            this.shareScreenButton.TabIndex = 6;
            this.shareScreenButton.UseVisualStyleBackColor = true;
            // 
            // sendButton
            // 
            this.sendButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sendButton.Enabled = false;
            this.sendButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendButton.Image = ((System.Drawing.Image)(resources.GetObject("sendButton.Image")));
            this.sendButton.Location = new System.Drawing.Point(1199, 3);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(60, 48);
            this.sendButton.TabIndex = 5;
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendMessage);
            // 
            // serverMessageTextBox
            // 
            this.serverMessageTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverMessageTextBox.Enabled = false;
            this.serverMessageTextBox.Location = new System.Drawing.Point(3, 0);
            this.serverMessageTextBox.Name = "serverMessageTextBox";
            this.serverMessageTextBox.Size = new System.Drawing.Size(1124, 54);
            this.serverMessageTextBox.TabIndex = 4;
            this.serverMessageTextBox.Text = "";
            // 
            // ServerChatSectionTabs
            // 
            this.ServerChatSectionTabs.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.ServerChatSectionTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ServerChatSectionTabs.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.ServerChatSectionTabs.ImageList = this.imageList;
            this.ServerChatSectionTabs.ItemSize = new System.Drawing.Size(50, 200);
            this.ServerChatSectionTabs.Location = new System.Drawing.Point(3, 60);
            this.ServerChatSectionTabs.Multiline = true;
            this.ServerChatSectionTabs.Name = "ServerChatSectionTabs";
            this.ServerChatSectionTabs.SelectedIndex = 0;
            this.ServerChatSectionTabs.Size = new System.Drawing.Size(1259, 531);
            this.ServerChatSectionTabs.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.ServerChatSectionTabs.TabIndex = 4;
            this.ServerChatSectionTabs.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.AlignAndPaintTabs);
            this.ServerChatSectionTabs.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Offline.png");
            this.imageList.Images.SetKeyName(1, "Online.png");
            // 
            // messageDisplayArea
            // 
            this.messageDisplayArea.Location = new System.Drawing.Point(0, 0);
            this.messageDisplayArea.Name = "messageDisplayArea";
            this.messageDisplayArea.Size = new System.Drawing.Size(100, 96);
            this.messageDisplayArea.TabIndex = 0;
            this.messageDisplayArea.Text = "";
            // 
            // ServerChatScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 673);
            this.Controls.Add(this.ServerChatSectionTabs);
            this.Controls.Add(this.chatFooterPanel);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.statusBar);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ServerChatScreen";
            this.Text = "ServerUI";
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.appLogoImageBox)).EndInit();
            this.chatFooterPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// Defines the statusBar
        /// </summary>
        private System.Windows.Forms.StatusStrip statusBar;

        /// <summary>
        /// Defines the toolStripLabel
        /// </summary>
        private System.Windows.Forms.ToolStripStatusLabel toolStripLabel;

        /// <summary>
        /// Defines the headerPanel
        /// </summary>
        private System.Windows.Forms.Panel headerPanel;

        /// <summary>
        /// Defines the chatFooterPanel
        /// </summary>
        private System.Windows.Forms.Panel chatFooterPanel;

        /// <summary>
        /// Defines the serverMessageTextBox
        /// </summary>
        private System.Windows.Forms.RichTextBox serverMessageTextBox;

        /// <summary>
        /// Defines the portLabel
        /// </summary>
        private System.Windows.Forms.Label portLabel;

        /// <summary>
        /// Defines the portTextBox
        /// </summary>
        private System.Windows.Forms.TextBox portTextBox;

        /// <summary>
        /// Defines the portListenButton
        /// </summary>
        private System.Windows.Forms.Button portListenButton;

        /// <summary>
        /// Defines the ServerChatSectionTabs
        /// </summary>
        private System.Windows.Forms.TabControl ServerChatSectionTabs;

        /// <summary>
        /// Defines the sendButton
        /// </summary>
        private System.Windows.Forms.Button sendButton;

        /// <summary>
        /// Defines the shareScreenButton
        /// </summary>
        private System.Windows.Forms.Button shareScreenButton;

        /// <summary>
        /// Defines the imageList
        /// </summary>
        private System.Windows.Forms.ImageList imageList;

        /// <summary>
        /// Defines the messageDisplayArea
        /// </summary>
        private System.Windows.Forms.RichTextBox messageDisplayArea;

        private System.Drawing.Color getColor(int r, int g, int b)
        {
            return System.Drawing.Color.FromArgb(((int)(((byte)(r)))), ((int)(((byte)(g)))), ((int)(((byte)(b)))));
        }

        private System.Windows.Forms.Label appLogoLabel;
        private System.Windows.Forms.PictureBox appLogoImageBox;
    }
}
