﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      Polu Varshith
// </reviewer>
// <date>
//      07-Nov-2018
// </date>
// <summary>
//      Functionalities of UI design
// </summary>
// <copyright file="ServerChatScreen.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServerChatScreen"/> class.
        /// </summary>
        public ServerChatScreen()
        {
            this.InitializeComponent();
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "trigger.txt";
            watcher.Changed += new FileSystemEventHandler(this.OnMessageReceived);
            watcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// The InvokeMessageDelegate
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="isClientMessage">The isClientMessage<see cref="string"/></param>
        internal delegate void InvokeMessageDelegate(string clientName, string message, string isClientMessage);

        /// <summary>
        /// The OnMessageReceived
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="FileSystemEventArgs"/></param>
        internal void OnMessageReceived(object source, FileSystemEventArgs e)
        {
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + "messages.txt";
            try
            {
                string[] lines = File.ReadAllLines(path);
                if (lines.Length > 1)
                {
                    this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), lines[0], lines[1], lines[2]);
                }
            }
            catch (Exception)
            {
                // Handle the exception
            }
        }

        /// <summary>
        /// The InvokeMessage
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="isClientMessage">The isClientMessage<see cref="string"/></param>
        internal void InvokeMessage(string clientName, string message, string isClientMessage)
        {
            Console.WriteLine("Invoke message");
            TabPage messagePage = null;
            TextBox unreadMessageCountTextBox = null;
            TextBox tabPageTop = null;
            TabControl.TabPageCollection pages = ServerChatSectionTabs.TabPages;

            foreach (TabPage page in pages)
            {
                if (page.Name.Equals(clientName + "tabPage"))
                {
                    messagePage = page;
                    break;
                }
            }
            Console.WriteLine("Page not found out");
            int tabPageNextTopHeight = 0;
            if (messagePage == null)
            {
                messagePage = CreateMessagePage(clientName);
                ServerChatSectionTabs.TabPages.Add(messagePage);
            }

            Control.ControlCollection controls = messagePage.Controls;
            foreach (Control control in controls)
            {
                if (control.Name.Equals(clientName + "unreadMessage"))
                {
                    unreadMessageCountTextBox = control as TextBox;
                }
                if (control.Name.Equals(clientName + "tabPageTop"))
                {
                    tabPageTop = control as TextBox;
                    tabPageNextTopHeight = int.Parse(tabPageTop.Text);
                }
            }

            TabPage currentPage = ServerChatSectionTabs.SelectedTab;

            try
            {
                ServerChatSectionTabs.TabPages.Remove(messagePage);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }

            ServerChatSectionTabs.TabPages.Insert(0, messagePage);
            ServerChatSectionTabs.SelectTab(ServerChatSectionTabs.TabPages.IndexOf(currentPage));

            Console.WriteLine("Tab page insertion deletion select tab successful");

            string unreadMessageCount = unreadMessageCountTextBox.Text;
            string currentClientName = messagePage.Text;
            int count = int.Parse(unreadMessageCount);
            if (isClientMessage.Equals("Client"))
            {
                count += 1;
            }
            string strOfCount = count.ToString();
            string updatedCount = "";

            if (strOfCount.Length == 1)
            {
                updatedCount = "0" + strOfCount;
            }
            else if (strOfCount.Length == 2)
            {
                updatedCount = strOfCount;
            }
            else
            {
                updatedCount = "99";
            }
            Console.WriteLine("Count update successful");
            unreadMessageCountTextBox.Text = updatedCount;

            string tabPageNewHeight = createMessageTextBox(isClientMessage, tabPageNextTopHeight, messagePage, message);
            tabPageTop.Text = tabPageNewHeight;
        }

        /// <summary>
        /// The createMessageTextBox
        /// </summary>
        /// <param name="isClientMessage">The isClientMessage<see cref="string"/></param>
        /// <param name="tabPageNextTopHeight">The tabPageNextTopHeight<see cref="int"/></param>
        /// <param name="messagePage">The messagePage<see cref="TabPage"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        /// <returns>The <see cref="string"/></returns>
        private string createMessageTextBox(string isClientMessage, int tabPageNextTopHeight, TabPage messagePage, string message)
        {
            RichTextBox messageBox = new RichTextBox();
            messageBox.Font = new Font("Lucinda Console", 12);
            float adjustedWidth = (float)0.7 * messagePage.Width;
            float shiftedWidth = messagePage.Width - (float)0.7 * messagePage.Width;
            messageBox.Width = (int)adjustedWidth;
            messageBox.Height = 30;
            messageBox.ReadOnly = true;
            messageBox.ContentsResized += new ContentsResizedEventHandler(TextBoxContentResized);

            int prevVerticalLocation = messagePage.VerticalScroll.Value;
            int prevHorizontalLocation = messagePage.HorizontalScroll.Value;
            messagePage.VerticalScroll.Value = 0;
            messagePage.HorizontalScroll.Value = 0;

            if (isClientMessage.Equals("Server"))
            {
                messageBox.Location = new Point((int)shiftedWidth, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Blue;
            }
            else if (isClientMessage.Equals("Client"))
            {
                messageBox.Location = new Point(0, tabPageNextTopHeight + 5);
                messageBox.ForeColor = Color.Orange;
            }
            messageBox.AppendText(message);
            messageBox.AppendText(System.Environment.NewLine);
            messageBox.Select(0, message.Length);
            messageBox.SelectionAlignment = HorizontalAlignment.Left;
            int prevLength = messageBox.TextLength;
            string time = DateTime.Now.ToString("HH:mm:ss tt");
            messageBox.AppendText(time);
            messageBox.Select(prevLength, time.Length);
            Console.WriteLine("Selected Text" + messageBox.SelectedText);
            messageBox.SelectionFont = new Font("Lucinda Console", 6);
            messageBox.SelectionAlignment = HorizontalAlignment.Right;

            messagePage.Controls.Add(messageBox);
            tabPageNextTopHeight += (messageBox.Height + 5);


            Console.WriteLine("Locations After New MessageBox " + messagePage.VerticalScroll.Value + " " + messagePage.HorizontalScroll.Value);
            messagePage.VerticalScroll.Value = prevVerticalLocation;
            messagePage.HorizontalScroll.Value = prevHorizontalLocation;
            Console.WriteLine("Locations After New MessageBox(Updated) " + messagePage.VerticalScroll.Value + " " + messagePage.HorizontalScroll.Value);
            return tabPageNextTopHeight.ToString();
        }

        /// <summary>
        /// The CreateMessagePage
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <returns>The <see cref="TabPage"/></returns>
        private TabPage CreateMessagePage(string clientName)
        {
            TabPage messagePage = new TabPage();
            messagePage.Name = clientName + "tabPage";
            messagePage.Text = clientName;
            messagePage.AutoScroll = true;
            messagePage.HorizontalScroll.Enabled = false;
            messagePage.VerticalScroll.Enabled = true;
            messagePage.Size = new Size(ServerChatSectionTabs.Width, ServerChatSectionTabs.Height);

            TextBox unreadMessageCountTextBox = new TextBox();
            unreadMessageCountTextBox.Name = clientName + "unreadMessage";
            unreadMessageCountTextBox.Height = this.ServerChatSectionTabs.Height;
            unreadMessageCountTextBox.Width = this.ServerChatSectionTabs.Width;
            unreadMessageCountTextBox.Visible = false;
            unreadMessageCountTextBox.Text = "00";

            TextBox tabPageTop = new TextBox();
            tabPageTop.Height = 100;
            tabPageTop.Width = 50;
            tabPageTop.Name = clientName + "tabPageTop";
            tabPageTop.Text = "0";
            tabPageTop.Visible = false;

            messagePage.Controls.Add(unreadMessageCountTextBox);
            messagePage.Controls.Add(tabPageTop);

            return messagePage;
        }

        /// <summary>
        /// The TextBoxContentResized
        /// </summary>
        /// <param name="Sender">The Sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ContentsResizedEventArgs"/></param>
        private void TextBoxContentResized(object Sender, ContentsResizedEventArgs e)
        {
            ((RichTextBox)Sender).Height = e.NewRectangle.Height + 5;
        }

        /// <summary>
        /// The AlignAndPaintTabs
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="DrawItemEventArgs"/></param>
        private void AlignAndPaintTabs(object sender, DrawItemEventArgs e)
        {
            Console.WriteLine("Started");
            Graphics g = e.Graphics;
            Brush textBrush;

            if (e.Index >= ServerChatSectionTabs.TabPages.Count)
            {
                return;
            }

            TabPage tabPage = ServerChatSectionTabs.TabPages[e.Index];
            tabPage.BorderStyle = BorderStyle.None;
            Rectangle tabBounds = ServerChatSectionTabs.GetTabRect(e.Index);
            Rectangle unreadMessageCountBounds = new Rectangle(180, tabBounds.Y + 28, 30, 30);
            Brush gray = new SolidBrush(System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245))))));
            Brush graySelected = new SolidBrush(System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225))))));

            if (e.State == DrawItemState.Selected)
            {
                g.FillRectangle(graySelected, e.Bounds);
            }
            else
            {
                g.FillRectangle(gray, e.Bounds);
            }
            Console.WriteLine("Brushes and bounds fixed");
            Color violet = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(29)))), ((int)(((byte)(58)))));
            textBrush = new SolidBrush(violet);
            Font tabFontActive = new Font("Segoe UI", (float)15.0, FontStyle.Bold, GraphicsUnit.Pixel);
            Font tabFont = new Font("Segoe UI", (float)15.0, FontStyle.Regular, GraphicsUnit.Pixel);
            Font unreadMessageFont = new Font("Segoe UI", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            StringFormat stringFlags = new StringFormat();
            stringFlags.Alignment = StringAlignment.Center;
            stringFlags.LineAlignment = StringAlignment.Center;

            string tabPageName = tabPage.Text;
            Control[] list = tabPage.Controls.Find(tabPageName + "unreadMessage", true);
            string unreadMessageCount = list[0].Text;
            Console.WriteLine("Appropriate strings found out");
            if (e.State == DrawItemState.Selected)
            {
                list[0].Text = "00";
                g.DrawString(tabPageName, tabFontActive, textBrush, tabBounds, new StringFormat(stringFlags));
                g.DrawString("00", unreadMessageFont, textBrush, unreadMessageCountBounds, new StringFormat(stringFlags));
            }
            else
            {
                g.DrawString(tabPageName, tabFont, textBrush, tabBounds, new StringFormat(stringFlags));
                g.DrawString(unreadMessageCount, unreadMessageFont, textBrush, unreadMessageCountBounds, new StringFormat(stringFlags));
            }
        }

        /// <summary>
        /// The PortNumberCharacterEntry
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="KeyPressEventArgs"/></param>
        private void PortNumberCharacterEntry(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        /// <summary>
        /// The SelectedIndexChanged
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage tabPage = ServerChatSectionTabs.SelectedTab;
            string tabPageName = tabPage.Text;

            Control[] list = tabPage.Controls.Find(tabPageName + "unreadMessage", true);
            string unreadMessageCount = "00";
            list[0].Text = unreadMessageCount;
        }

        /// <summary>
        /// The PortButtonClick
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void PortButtonClick(object sender, EventArgs e)
        {
            //do something
            bool isPortNumberValid = true;
            if (isPortNumberValid)
            {
                shareScreenButton.Enabled = true;
                sendButton.Enabled = true;
                serverMessageTextBox.Enabled = true;
            }
        }

        private void sendMessage(object sender, EventArgs e)
        {
            bool messageSent = true;
            //Do something

            if (messageSent)
            {
                string clientName = ServerChatSectionTabs.SelectedTab.Name;
                clientName = clientName.Substring(0, clientName.Length - 7);
                InvokeMessage(clientName, serverMessageTextBox.Text, "Server");
                serverMessageTextBox.Text = "";
            }
        }
    }
}
