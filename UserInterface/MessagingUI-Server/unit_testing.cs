namespace MessagingUIServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows.Forms;

    public class Unit_testing
    {
        public delegate void InvokeMessageDelegate(string clientName, string message, string isClientMessage);
       /* static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ServerChatScreen x = new ServerChatScreen();
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler((source, e) => OnTimedEvent(e, x));
            aTimer.Interval = 1000;
            aTimer.Enabled = true;
            Application.Run(x);

        }*/
        private static void OnTimedEvent(ElapsedEventArgs e, ServerChatScreen x)
        {
            ////string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + "messages.txt";
            try
            {
                Tuple<string, string, string>[] testTupleArray = new Tuple<string, string, string>[10];
                testTupleArray[0] = new Tuple<string, string, string>("user1", "u1Message", "client");
                testTupleArray[1] = new Tuple<string, string, string>("user2", "u2Message", "client");
                testTupleArray[2] = new Tuple<string, string, string>("user3", "u3Message", "client");
                testTupleArray[3] = new Tuple<string, string, string>("user4", "u4Message", "client");
                testTupleArray[4] = new Tuple<string, string, string>("user5", "u5Message", "client");
                testTupleArray[5] = new Tuple<string, string, string>("user1", "u1Message", "Server");
                testTupleArray[6] = new Tuple<string, string, string>("user2", "u2Message", "Server");
                testTupleArray[7] = new Tuple<string, string, string>("user3", "u3Message", "Server");
                testTupleArray[8] = new Tuple<string, string, string>("user4", "u4Message", "Server");
                testTupleArray[9] = new Tuple<string, string, string>("user5", "u5Message", "Server");

                ////string[] lines = File.ReadAllLines(path);
                if (true)
                {
                    Random randomInteger = new Random();
                    int randomNumber = randomInteger.Next(0, 10);
                    x.BeginInvoke(new InvokeMessageDelegate(x.InvokeMessage), testTupleArray[randomNumber].Item1, testTupleArray[randomNumber].Item2, testTupleArray[randomNumber].Item3);
                }
            }
            catch (Exception)
            {
                // Handle the exception
            }
        }


    }
}
