﻿//-----------------------------------------------------------------------
// <author> 
//     Durga Prasad Reddy. K
// </author>
//
// <date> 
//     09-11-2018 
// </date>
// 
// <reviewer> 
//     
// </reviewer>
// 
// <copyright file="Unit_testing.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is unit testing for client side messaging.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows.Forms;

    public class Unit_testing
    {
        public delegate void InvokeMessageDelegate(string isClientMessage, string message, string messageSentStatus);
        /*static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ClientChatServer x = new ClientChatServer();
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler((source, e) => OnTimedEvent(e, x));
            aTimer.Interval = 1000;
            aTimer.Enabled = true;
            Application.Run(x);

        }*/
        private static void OnTimedEvent(ElapsedEventArgs e, ClientChatServer x)
        {
            ////string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + "messages.txt";
            try
            {
                Tuple<string, string>[] testTupleArray = new Tuple<string, string>[10];
                testTupleArray[0] = new Tuple<string, string>("Client", "u1msg");
                testTupleArray[1] = new Tuple<string, string>("Server", "u1msg");
                testTupleArray[2] = new Tuple<string, string>("Client", "u2msg");
                testTupleArray[3] = new Tuple<string, string>("Server", "u2msg");
                testTupleArray[4] = new Tuple<string, string>("Client", "u3msg");
                testTupleArray[5] = new Tuple<string, string>("Server", "u3msg");
                testTupleArray[6] = new Tuple<string, string>("Client", "u4msg");
                testTupleArray[7] = new Tuple<string, string>("Server", "u4msg");
                testTupleArray[8] = new Tuple<string, string>("Client", "u5msg");
                testTupleArray[9] = new Tuple<string, string>("Server", "u5msg");
                /* testTupleArray[1] = new Tuple<string, string, string>("user2", "u2Message", "Client");
                 testTupleArray[2] = new Tuple<string, string, string>("user3", "u3Message", "Client");
                 testTupleArray[3] = new Tuple<string, string, string>("user4", "u4Message", "Client");
                 testTupleArray[4] = new Tuple<string, string, string>("user5", "u5Message", "Client");
                 testTupleArray[5] = new Tuple<string, string, string>("user1", "u1Message", "Server");
                 testTupleArray[6] = new Tuple<string, string, string>("user2", "u2Message", "Server");
                 testTupleArray[7] = new Tuple<string, string, string>("user3", "u3Message", "Server");
                 testTupleArray[8] = new Tuple<string, string, string>("user4", "u4Message", "Server");
                 testTupleArray[9] = new Tuple<string, string, string>("user5", "u5Message", "Server");
                 */
                ////string[] lines = File.ReadAllLines(path);
                if (true)
                {
                    Random randomInteger = new Random();
                    int randomNumber = randomInteger.Next(0, 10);
                    x.BeginInvoke(new InvokeMessageDelegate(x.InvokeMessage), testTupleArray[randomNumber].Item1, testTupleArray[randomNumber].Item2);
                }
            }
            catch (Exception)
            {
                // Handle the exception
            }
        }


    }
}