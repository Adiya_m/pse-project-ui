﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Networking
{
    public enum StatusCode
    {
        Success,
        Failure
    };

    public enum DataType
    {
        Message,
        ImageSharing
    };

    // Prototype of function triggered when data is received
    public delegate void DataReceivalHandler(string data, IPAddress fromIP);
    
    // Prototype of function triggered to notify the status of data transfer
    public delegate void DataStatusHandler(ulong dataID, StatusCode status);

    public interface ICommunication
    {

        bool Send(string msg, ulong dataID, IPAddress targetIP, DataType type);
        bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler);
        bool SubscribeForDataStatus(DataType type, DataStatusHandler statusHandler);
    }

    // Interface for SingletoneFactory that produces ICommunication objects
    public interface ICommunicationFactory
    {
        ICommunication GetCommunicator();
    }

}
